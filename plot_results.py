#!/usr/bin/env python

### Run script in directory containing the log files

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys
import pandas as pd
import os


def get_file(patient_name, name = "controller", ext=".txt"):
    """
    Returns file name as a string
    name: controller, data and Pulse
    """
    #Get all text files in directory
    text_files = [file_ for file_ in os.listdir(".") if file_.endswith(ext)]

    # Filter out specific patient files
    patient_files = [patient for patient in text_files if patient_name in patient]

    # Grab log file that has either (name: "Pulse" or "Simulation") in filename
    str_files = [ file_ for file_ in patient_files if name in file_ ]
    str_file = "".join(map(str, str_files))

    return str_file


# function: to read the pulse log file
def read_pulse_logfile(file_name):
    df = pd.read_csv(file_name,
                     names = ["Time","SYS","DIA","MAP","HR","RR","OXY"],
                     header=0)
    return df


# function: to read and parse the simulation log file
def read_file(file_name, name="controller"):
    if name == "controller":
        df = pd.read_csv(file_name,
                         names=['date_time', 'name', 'level', 'message'],
                         sep="\s-\s",
                         engine="python",
                         header=0,
                         )
    elif name == "data":
        df = pd.read_csv(file_name,
                         names = ['Time', 'BP_DRUG_RATE','SALINE_RATE'],
                         header=0,
                        )
    return df

# function to parse the controller log file
def parse_log_file(log_file):
    patient_data = {}

    # contains only date_time and message columns
    datetime_messages = log_file.iloc[:,[0,3]]

    # mask messages with MAP value
    idx = datetime_messages['message'].str.contains("MAP:")

    # filter messges with MAP value
    datetime_messages = datetime_messages[idx]

    # extract MAP from messages
    messages = datetime_messages['message']
    MAP_messages = [ message for message in messages if "MAP:" in message ]

    # place both MAP and date time into dictionary
    patient_data['MAP'] = [float(value.split(':')[1]) for value in MAP_messages]
    patient_data['datetime'] = datetime_messages['date_time']

    assert len(patient_data['MAP']) == len(patient_data['datetime'])

    return patient_data


# get patient data for 'patient'
def get_patient_data(patient):

    # conversion from seconds to minutes
    SECS_PER_MIN = 60

    # patient data dictionary
    sim_data = {}
    patient_data = {}

    # get the log and data files
    g_log_file = get_file(patient, "controller")
    g_pump_file = get_file(patient, "data")
    g_pulse_file = get_file(patient, "Pulse")

    # read str_files
    log_file = read_file(g_log_file, "controller")
    pump_file = read_file(g_pump_file, "data")
    pulse_file = read_pulse_logfile(g_pulse_file)

    # parse log file and get patient data
    controller_data = parse_log_file(log_file)

    sim_data['patient'] = {
                          "time" : pulse_file['Time']/SECS_PER_MIN,
                           "MAP"  : pulse_file["MAP"],
                          }

    sim_data["pump"] =  {
                        "time" : pump_file["Time"]/SECS_PER_MIN,
                        "rate-change" : pump_file["BP_DRUG_RATE"].values,
                        }

    sim_data["controller"] = {
                          "time" : pump_file["Time"]/SECS_PER_MIN,
                          "MAP" : controller_data["MAP"],
                          }

    return sim_data

# plot patient data
def plot_data(patient, sim_data, separate=True):

    matplotlib.rcParams.update({'font.size': 10})

    # Plot all data (one-axis per entity)
    fig = plt.figure()
    fig.canvas.set_window_title(patient)
    if separate:
        plot_data_separate(sim_data)
    else:
        plot_data_condensed(sim_data)

def plot_data_separate(sim_data):
    # TO-DO: fix the separate plot
    pass


def plot_data_condensed(sim_data):
    # global plot parameters
    num_entities = 2

    pump_plot_num = 1
    patient_plot_num = 2

    time_min = 0
    time_max = 65

    MAP_min = 65
    MAP_max = 95

    rate_min = 0
    rate_max = 90

    cla_not_started_x_range = [0, sim_data["pump"]["time"][0]]

    # pump plot
    ax = plt.subplot(num_entities, 1, pump_plot_num)

    plt.step(sim_data["pump"]["time"],
             sim_data["pump"]["rate-change"],
             'r',
             where='post'
             )

    # fill area where patient is not yet in ICU
    ax.fill_between(cla_not_started_x_range,
                    rate_min,
                    rate_max,
                    facecolor="tab:gray",
                    alpha=0.25)

    # add text about CLA not started
    text_x = 1 # min
    text_y = 20 # ml/hr
    # ax.text(text_x,text_y, "patient not in ICU\nCLA not started",)

    plt.xlim(time_min, time_max)
    plt.ylim(rate_min, rate_max)

    plt.title("CLA Reaction")
    plt.ylabel("Infusion\nRate\n(ml/hr)")

    plt.grid()


    # patient plot
    target_max = 80
    target_min = 75
    ax = plt.subplot(num_entities, 1, patient_plot_num)
    plt.plot(sim_data["patient"]["time"],sim_data["patient"]["MAP"], 'k', label='patient')
    plt.plot(sim_data["controller"]["time"],sim_data["controller"]["MAP"], 'r', label='algorithm view')

    plt.legend()

    plt.xlim(time_min, time_max)
    plt.ylim(MAP_min, MAP_max)

    target_max_line = ax.axhline(y=target_max)
    target_min_line = ax.axhline(y=target_min)

    xdata = np.array(target_max_line.get_xdata())*time_max
    #print(xdata)

    ax.fill_between(xdata,
                    target_max_line.get_ydata(),
                    target_min_line.get_ydata(),
                    facecolor='green',
                    alpha=0.25)

    ax.fill_between(cla_not_started_x_range,
                    MAP_min,
                    MAP_max,
                    facecolor="tab:gray",
                    alpha=0.25)

    plt.title("Patient Data")
    plt.xlabel("time (min)")
    plt.ylabel("MAP\n(mmHg)")

    plt.grid()

    plt.tight_layout()
    plt.show()


# ------------------------------------------------------------------------------
def print_usage():
    print("usage: script patients")

if __name__ == "__main__":

    # plot only condensed for now
    patient_pos = 1
    separate = True
    if len(sys.argv) > patient_pos:

        patients = sys.argv[patient_pos:]

        print(patients)

        for patient in patients:
            print(patient)
            sim_data = get_patient_data(patient)
            plot_data(patient, sim_data, False)


    else:
        print_usage()
        sys.exit(0)
