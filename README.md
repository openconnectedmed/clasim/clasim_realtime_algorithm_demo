# Demo Hypotension Management Algorithm for CLASim Real-Time Framework

This is a ROS package that implements a demo hypotension management algorithm to interact with the current demo patient behavior in the <a href="https://gitlab.com/openconnectedmed/clasim/clasim_realtime" target="_blank">CLASim Real-Time Framework</a>. The algorithm that is implemented and the assumptions it makes about the patient states are document in <a href="https://digitalcommons.bucknell.edu/masters_theses/220/" target="_blank">Farooq Gessa's masters thesis work</a> and the <a href="https://gitlab.com/openconnectedmed/clasim/clasim_software_only/raw/master/Gessa-et-al-MCPS-2018-SIGBED-pub-version.pdf?inline=false" target="_blank">paper</a> we presented at the <a href="https://rtg.cis.upenn.edu/mcps-workshop-2018/#program" target="_blank">2018 Medical Cyber-Physical Systems Workshop</a>.

## Installation
This software has only been tested on Ubuntu 16.04 LTS (Xenial). This is the recommended platform. We hope to support other platforms soon.

This package relies assumes you are working with the <a href="https://gitlab.com/openconnectedmed/clasim/clasim_realtime" target="_blank">CLASim Real-Time Framework</a> so make sure you have followed the instructions to install it. It also depends on <a href="http://wiki.ros.org/kinetic" target="_blank">ROS Kinetic Kame</a>, which you should have installed as part of installing the CLASim Real-Time Framework.

To install the algorithm:
1. Copy the `physiology_algorithm` folder to the catkin workspace
   ~~~bash
   # assuming you are in the repository folder which contains the subfolder physiology_algorithm 
   $ cp -r physiology_algorithm <path-to-catkin-workspace>/src/
   ~~~
2. Install the package in the catkin workspace
   ~~~bash
   # make sure you are in the ROS environment
   $ source /opt/ros/kinetic/setup.bash
   # go to the catkin workspace top-level folder
   $ cd <path-to-catkin-workspace> # e.g. cd ~/catkin_ws
   # compile the catkin workspace
   $ catkin_make
   ~~~

## Running the Algorithm
The algorithm needs information from CLAsim to know when to begin interacting with the patient. The algorithm also assumes that the ROS master is running on local host. To run the algorithe properly you must follow these steps:

1. Launch ROSBridge
   ~~~bash
   # make sure you are in the ROS environment
   $ source /opt/ros/kinetic/setup.bash
   # install libconfig++
   $ roslaunch rosbridge_server rosbridge_tcp.launch
   ~~~
2. *In a new terminal*, launch the algorithm
   ~~~bash
   # make sure you are in the ROS environment
   $ source /opt/ros/kinetic/setup.bash
   # make sure you have the catkin workspace environment as well
   $ source <path-to-catkin-workspace>/devel/setup.bash # e.g source ~/catkin_ws/devel/setup.bash
   # ensure you are pointing to the right ROS master
   $ export ROS_MASTER_URI=http://localhost:11311/
   # run the algorithm
   $ rosrun physiology_algorithm controller_simulated_monitor.py
   ~~~
3. *In a new terminal*, launch the CLAsim Real-Time Framework
   ~~~bash
   # make sure you are in the ROS environment
   $ source /opt/ros/kinetic/setup.bash
   # ensure you are pointing to the right ROS master
   $ export ROS_MASTER_URI=http://localhost:11311/
   # go to the Pulse bin folder
   $ cd <path-to-Pulse>/builds/install/bin # e.g. cd ~/projects/physiology/builds/install/bin
   # run the CLAsim Real-Time Framework
   $ ./clasim_rt
   ~~~

The simulation is programmed to run an hour. The patient is first hemorraged to get them into a state that the alorithm can deal with. Once the patient starts interacting with the algorithm, you should see something like the following printing out in the terminal with the algorithm.
   
   ~~~bash
   $ rosrun physiology_algorithm controller_simulated_monitor.py
   monitor controller initiated: Algo1
   Patient's current MAP: 66.873622
   initializing infusions
   Patient's current MAP: 67.6021326667
   Patient's current MAP: 68.02179
   Patient's current MAP: 67.741105
   Patient's current MAP: 66.719755
   Patient's current MAP: 66.657793
   Patient's current MAP: 67.682248
   Patient's current MAP: 67.8381766667
   Patient's current MAP: 67.0220103333
   Patient's current MAP: 66.546031
   Patient's current MAP: 67.206081
   Patient's current MAP: 67.6615603333
   ...
   ~~~

## Plot the Results
The plotting script many of the libraries found in the <a href="https://docs.anaconda.com/anaconda/install/linux/" target="_blank">Anaconda python distribution</a>. We recommend you use this distribution. Once you have this installed, you can test the script to make sure all the libraries are available as follows
~~~bash
# test the script
$ <path-to-Anaconda-Python> <path-to-CLAsimRT>/plotresults.py
# this should pring the following if everything loads properly
usage: script  patients
~~~

The actual plotting part is currently a little messy. The following instructions should help.
1. Create a folder where you want to work with the results. We will call this folder `<results>`.
2. Go to `<path-to-CLASimRT>/results`. You will see the following files that you should copy to the results folder
   - `data_Gus<timestamp>.txt`
   - `PulseSimEngine_Gus<timestamp>.txt`

   `<timestamp>` is some date and time appended to the filename.
3. Go to `<path-to-catkin-workspace>/src/physiology_algorithm/scripts/logs/` and copy the file `Algo1_controller_simulated_monitor_<timestamp>.log`to the `<results>` folder.
4. Rename the `Algo1_controller_simulated_monitor_<timestamp>.log` to add the patient name `Gus` to the file name right before the timestamp so that it is `Algo1_controller_simulated_monitor_Gus<timestamp>.log`
5. Go to the `<results>` folder and run the script.
   ~~~bash
   # to plot make sure you are in the results folder you create
   $ cd <path-to-results>
   # to plot the simulation data select one or more patients
   # the default patient is Gus
   $ <path-to-Anaconda-Python> <path-to-CLAsimRT>/plotresults.py Gus
   ~~~