import logging
import time
import os


path = os.path.dirname(__file__)

def setup_logger(logger_name, logging_level=logging.DEBUG):
    # Set up logger
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

    # create timestamp to append to filename
    timestamp = time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime())

    # create logs folder within package
    create_directory('logs')

    # get the full path to logs directory
    logs_path = get_directory_path('logs')

    # create a unique filename
    filename = logs_path +"Algo1_"+logger_name+"_"+timestamp+".log"

    # file handler
    handler = logging.FileHandler(filename, mode="w")
    handler.setLevel(logging_level)

    # formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    return logger


def create_directory(name):
    global path
    full_path = os.path.join(path, name)

    try:
        if not os.path.isdir(full_path):
            os.makedirs(full_path)
    except OSError:
        pass


def get_directory_path(name):
    global path
    full_path = os.path.join(path, name)
    return full_path + "/"
