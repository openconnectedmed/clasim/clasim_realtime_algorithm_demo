#!/usr/bin/env python

import time
import rospy
from abc import ABCMeta, abstractmethod
from std_msgs.msg import String, Float64


class Controller(object):
    """ Base class for hypotention Management Algorithm
    """
    __metaclass__ = ABCMeta

    def __init__(self, logger):
        self.rate_pub = rospy.Publisher("/pumpin", String, queue_size=10)
        self.rate_pub_out = rospy.Publisher("/bpdrug_pumpout", Float64, queue_size=10)
        self.rate_pub_out_saline = rospy.Publisher("/saline_pumpout", Float64, queue_size=10)
        self.control_sub = rospy.Subscriber("/control", String, self.handle_message, queue_size=1)
        self.current_norepi_infusion_rate = 0.0
        self.saline_infusion_rate = 50.0
        self.saline_pump_volume = 500
        self.bpdrug_pump_volume = 500
        self.max_norepi_infusion_rate = 84.0
        self.target_MAP_range_max = 80.0
        self.target_MAP_range_min = 75.0
        self.MAP_increase_threshold = 80.0
        self.percent_of_rate = 0.75
        self.current_MAP = 0.0
        self.respond_to_data = self.initialize_infusions
        self.next_update_time = 0
        self.current_time = 0
        self._WAIT_10_MIN = 60 * 10
        self._WAIT_5_MIN = 60 * 5
        self.trigger = None
        self.logger = logger


    def handle_message(self, data):
        """ update the trigger variable whenever there is new message
            on control channel in order to switch to appropriate state
        """
        self.trigger = data.data

        if self.trigger == "stop":
            self.stop()

        if self.trigger is None:
            self.wait()


    @abstractmethod
    def update(self, data):
        pass


    @abstractmethod
    def run(self, data):
        pass


    def stop(self):
        """ stop state """
        self.logger.info("Algorithm in 'stop' state")
        print("Algorithm in 'stop' state")
        self.reset_parameters()
        self.stop_pump()


    def reset_parameters(self):
        """ set parameters to intial state """
        print("variables have been initialized")
        self.logger.info("variables have been initialize")
        self.current_norepi_infusion_rate = 0.0
        self.saline_infusion_rate = 50.0
        self.saline_pump_volume = 500
        self.bpdrug_pump_volume = 500
        self.max_norepi_infusion_rate = 84.0
        self.target_MAP_range_max = 80.0
        self.target_MAP_range_min = 75.0
        self.MAP_increase_threshold = 80.0
        self.percent_of_rate = 0.75
        self.current_MAP = 0.0
        self.respond_to_data = self.initialize_infusions
        self.next_update_time = 0
        self.current_time = 0
        self._WAIT_10_MIN = 60 * 10
        self._WAIT_5_MIN = 60 * 5
        self.trigger = None


    def wait(self):
       """ do nothing state while waiting for new trigger """
       print("Algorithm in 'wait' state")
       self.logger.info("Algorithm in 'wait' state")


    def stop_pump(self):
        self.rate_pub.publish("stop")
        self.logger.info("pump stopped")


    def initialize_infusions(self):
        print("initializing infusions")
        self.logger.info("initializing infusions")
        self.current_norepi_infusion_rate = self.max_norepi_infusion_rate
        self.rate_pub.publish("start:{},{}".format(int(self.current_norepi_infusion_rate),self.bpdrug_pump_volume))
        self.logger.info("BP_drug pump initialized to Rate {} mL/hr, Volume {} mL".format(self.current_norepi_infusion_rate, self.bpdrug_pump_volume))
        self.rate_pub_out.publish(self.current_norepi_infusion_rate)
        self.rate_pub_out_saline.publish(self.saline_infusion_rate)
        self.logger.info("Saline pump rate initialized to Rate {} mL/hr, Volume {} mL".format(self.saline_infusion_rate,self.saline_pump_volume))
        self.next_update_time = self.current_time + self._WAIT_10_MIN
        self.respond_to_data = self.wait_for_MAP_increase


    def wait_for_MAP_increase(self):
        if (self.current_time >= self.next_update_time):
            self.logger.info("Waiting for MAP increase")

            if (self.current_MAP > self.MAP_increase_threshold):
                self.logger.info("Current MAP is greater than increase threshold")
                self.current_norepi_infusion_rate = self.percent_of_rate * self.max_norepi_infusion_rate
                self.rate_pub.publish("setrate:{}".format(int(self.current_norepi_infusion_rate)))
                self.logger.debug("BP_drug pump Rate set to {} mL/hr".format(self.current_norepi_infusion_rate))
                self.rate_pub_out.publish(self.current_norepi_infusion_rate)
                self.next_update_time = self.current_time + self._WAIT_10_MIN
                self.respond_to_data = self.ensure_MAP_decline_after_increase

            else:
                self.logger.info("Current MAP is less than increase threshold")
                self.next_update_time = self.current_time + self._WAIT_5_MIN


    def ensure_MAP_decline_after_increase(self):
        if (self.current_time >= self.next_update_time):
            self.logger.info("Ensuring MAP declines")

            if (self.current_MAP > self.MAP_increase_threshold):
                self.logger.info("Current MAP is greater than increase threshold")
                self.current_norepi_infusion_rate *= self.percent_of_rate
                self.rate_pub.publish("setrate:{}".format(int(self.current_norepi_infusion_rate)))
                self.logger.debug("BP_drug pump Rate set to {} mL/hr".format(self.current_norepi_infusion_rate))
                self.rate_pub_out.publish(self.current_norepi_infusion_rate)
                self.next_update_time = self.current_time + self._WAIT_10_MIN

            else:
                self.next_update_time = self.current_time + self._WAIT_10_MIN
                self.respond_to_data = self.maintain_MAP_in_range
                self.logger.info("Current MAP is less than increase threshold")


    def maintain_MAP_in_range(self):
        if (self.current_time >= self.next_update_time):
            self.logger.info("Maintaining MAP in range")

            if (self.current_MAP > self.target_MAP_range_max):
                self.logger.info("Current MAP is greater than target MAP range max")
                self.current_norepi_infusion_rate *= self.percent_of_rate
                self.rate_pub.publish("setrate:{}".format(int(self.current_norepi_infusion_rate)))
                self.logger.debug("BP_drug pump Rate set to {} mL/hr".format(self.current_norepi_infusion_rate))
                self.rate_pub_out.publish(self.current_norepi_infusion_rate)
                self.rate_pub_out.publish(self.current_norepi_infusion_rate)
                self.next_update_time = self.current_time + self._WAIT_10_MIN

            elif (self.current_MAP < self.target_MAP_range_min):
                self.logger.info("Current MAP is less than target MAP range min")
                self.current_norepi_infusion_rate = self.percent_of_rate * self.max_norepi_infusion_rate
                self.rate_pub.publish("setrate:{}".format(int(self.current_norepi_infusion_rate)))
                self.logger.debug("BP_drug pump Rate set to {} mL/hr".format(self.current_norepi_infusion_rate))
                self.rate_pub_out.publish(self.current_norepi_infusion_rate)
                self.next_update_time = self.current_time + self._WAIT_5_MIN

            else:
                self.logger.info("Current MAP is within target range")
                self.next_update_time = self.current_time + self._WAIT_10_MIN
