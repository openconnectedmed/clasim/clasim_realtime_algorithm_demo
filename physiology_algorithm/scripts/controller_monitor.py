#!/usr/bin/env python

import time
import rospy
from std_msgs.msg import String, Float64
from setup_logger import setup_logger
from Controller import Controller

logger = setup_logger('controller_monitor')

class MonitorController(Controller):
    """ This class implements an algorithm to manage a patient with hypotension;
        subscribes to the /monitor topic for patient data and publishes commands
        to change infusion rate to /pumpin topic
    """
    def __init__(self):
        super(MonitorController, self).__init__(logger)
        print("monitor controller initiated: Algo1")
        logger.info("monitor contoller initiated: Algo1")
        rospy.init_node("monitor_controller")
        self.monitor_sub = rospy.Subscriber("/monitor", String, self.update, queue_size=10)


    def update(self, data):
        if self.trigger == "start":
            self.run(data)


    def run(self, data):
        """ active state """
        logger.info("Algorithm in 'active' state")
        self.current_time = int(time.time())
        self.current_MAP = self.extract_MAP(data.data)
	print("Patient's current MAP: {}".format(self.current_MAP))
        logger.debug("Patient's current MAP: {}".format(self.current_MAP))
        self.respond_to_data()


    def extract_MAP(self, monitor_data):
        # remove both opening and closing square brackets
    	monitor_data = monitor_data[1:-1]
        # convert data to a list based on the string separator
        list_monitor_data = monitor_data.split(',')
        # MAP is at the 12th position (zero indexed)
        string_MAP = list_monitor_data[12]

        # SYS is at the 11th position (zero indexed)
        string_SYS = list_monitor_data[11]

        # DIAS is at the 13th position (zero indexed)
        string_DIAS = list_monitor_data[13]

        # convert string MAP to integer value
        MAP = int(string_MAP)
        SYS = int(string_SYS)
        DIAS = int(string_DIAS)
        logger.debug("Received: MAP {}".format(MAP))
        logger.debug("Received: SYS {}".format(SYS))
        logger.debug("Received: DIAS {}".format(DIAS))

        return MAP


if __name__ == "__main__":
    monitor = MonitorController()

    try:
        rospy.spin()

    except KeyboardInterrupt:
	logger.error("An error occured,monitor controller exited")

    finally:
        monitor.stop_pump()
