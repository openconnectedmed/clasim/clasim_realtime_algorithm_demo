#!/usr/bin/env python

import time
import rospy
from std_msgs.msg import String, Float64
from setup_logger import setup_logger
from Controller import Controller

logger = setup_logger('controller_simulated_monitor')

class SimulatedMonitorController(Controller):
    """ This class implements an algorithm to manage a patient with hypotension;
        subscribes to the /dummy_monitor topic for patient data and publishes
        commands to change infusion rate to /pumpin topic
    """
    def __init__(self):
        super(SimulatedMonitorController, self).__init__(logger)
        print("monitor controller initiated: Algo1")
        logger.info("monitor contoller initiated: Algo1")
        rospy.init_node("simulated_monitor_controller")
        self.monitor_sub = rospy.Subscriber("/dummy_monitor", String, self.update, queue_size=10)


    def update(self, data):
        if self.trigger == "start":
            self.run(data)


    def run(self, data):
        """ active state """
        logger.info("Algorithm in 'active' state")
        self.current_time = int(time.time())
        self.current_MAP = self.calculate_MAP(data.data)
        print("Patient's current MAP: {}".format(self.current_MAP))
        logger.debug("Patient's current MAP: {}".format(self.current_MAP))
        self.respond_to_data()


    def calculate_MAP(self, patient_ibp):
        ibp = patient_ibp.split("/")
        systolic = float(ibp[0])
        diastolic = float(ibp[1])
        self.logger.debug("Received: Systolic prssure {}, Diastolic pressure {}".format(systolic, diastolic))
        MAP = (1/3.0)*systolic + (2/3.0)*diastolic
        return MAP


if __name__ == "__main__":
    sim_monitor = SimulatedMonitorController()

    try:
        rospy.spin()

    except KeyboardInterrupt:
        logger.error("An error occured,simulated monitor controller exited")

    finally:
        sim_monitor.stop_pump()
